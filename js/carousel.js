var slides = document.getElementById("slides-text-image").children;

document.getElementById("change-slide-button-left").addEventListener("click", function(){
  changeSlides("left");
});

document.getElementById("change-slide-button-right").addEventListener("click", function(){
  changeSlides("right");
});

function changeSlides(direction){

    for (var i = 0; i < slides.length; i++){
      if (direction == "left"){
        if (slides[i].children[0].classList.contains("zero-position-slide")){

          slides[i].children[0].classList.remove("no-transition", "zero-position-slide");
          slides[i].children[0].classList.add("transition-text", "first-position-slide");

          slides[i].children[1].classList.remove("no-transition", "zero-position-slide");
          slides[i].children[1].classList.add("transition-img", "first-position-slide");

        } else if (slides[i].children[0].classList.contains("first-position-slide")){

          slides[i].children[0].classList.remove("first-position-slide");
          slides[i].children[0].classList.add("transition-text", "second-position-slide");

          slides[i].children[1].classList.remove("first-position-slide");
          slides[i].children[1].classList.add("transition-img", "second-position-slide");

        } else if (slides[i].children[0].classList.contains("second-position-slide")) {

          slides[i].children[0].classList.remove("second-position-slide");
          slides[i].children[0].classList.add("transition-text", "third-position-slide");

          slides[i].children[1].classList.remove("second-position-slide");
          slides[i].children[1].classList.add("transition-img", "third-position-slide");

        } else if (slides[i].children[0].classList.contains("third-position-slide")){

          slides[i].children[0].classList.remove("transition-text", "third-position-slide");
          slides[i].children[0].classList.add("no-transition", "zero-position-slide");

          slides[i].children[1].classList.remove("transition-img", "third-position-slide");
          slides[i].children[1].classList.add("no-transition", "zero-position-slide");

        }
      } else if (direction == "right"){
        if (slides[i].children[0].classList.contains("zero-position-slide")){

          slides[i].children[0].classList.remove("transition-text", "zero-position-slide");
          slides[i].children[0].classList.add("no-transition", "third-position-slide");

          slides[i].children[1].classList.remove("transition-img", "zero-position-slide");
          slides[i].children[1].classList.add("no-transition", "third-position-slide");

        } else if (slides[i].children[0].classList.contains("first-position-slide")){

          slides[i].children[0].classList.remove("first-position-slide");
          slides[i].children[0].classList.add("transition-text", "zero-position-slide");

          slides[i].children[1].classList.remove("first-position-slide");
          slides[i].children[1].classList.add("transition-img", "zero-position-slide");

        } else if (slides[i].children[0].classList.contains("second-position-slide")){

          slides[i].children[0].classList.remove("second-position-slide");
          slides[i].children[0].classList.add("transition-text", "first-position-slide");

          slides[i].children[1].classList.remove("second-position-slide");
          slides[i].children[1].classList.add("transition-img", "first-position-slide");
          
        } else if (slides[i].children[0].classList.contains("third-position-slide")){

          slides[i].children[0].classList.remove("third-position-slide", "no-transition");
          slides[i].children[0].classList.add("transition-text", "second-position-slide");

          slides[i].children[1].classList.remove("third-position-slide", "no-transition");
          slides[i].children[1].classList.add("transition-img", "second-position-slide");
        }
      }
    }
}